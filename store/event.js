export const state = () => ({
  id: '',
  title: '',
  startDate: '',
  endDate: '',
  description: '',
  userId: ''
})

export const mutations = {
  setId(state, item) {
    state.id = item
  },
  setTitle(state, item) {
    state.title = item
  },
  setStartDate(state, item) {
    state.startDate = item
  },
  setEndDate(state, item) {
    state.endDate = item
  },
  setDescription(state, item) {
    state.description = item
  },
  setUserId(state, item) {
    state.userId = item
  }
}

export const actions = {
  setTitle({ commit }, payload) {
    commit('setTitle', payload)
  },
  setStartDate({ commit }, payload) {
    commit('setStartDate', payload)
  },
  setEndDate({ commit }, payload) {
    commit('setEndDate', payload)
  },
  setDescription({ commit }, payload) {
    commit('setDescription', payload)
  },
  setUserId({ commit }, payload) {
    commit('setUserId', payload)
  },
  setEvent({ commit }, payload) {
    commit('setId', payload.id)
    commit('setTitle', payload.title)
    commit('setStartDate', payload.startDate)
    commit('setEndDate', payload.endDate)
    commit('setDescription', payload.description)
    commit('setUserId', payload.userId)
  },
  reset({ commit }, payload) {
    commit('setId', '')
    commit('setTitle', '')
    commit('setStartDate', '')
    commit('setEndDate', '')
    commit('setDescription', '')
    commit('setUserId', '')
  }
}
