export const state = () => ({
  items: []
})

export const mutations = {
  setItems(state, item) {
    item.forEach(user => (user.isSelected = false))
    state.items = item
  },
  setSelected(state, item) {
    const usersArr = [...state.items]

    usersArr.forEach((user) => {
      if (item === user.id) user.isSelected = true
      else user.isSelected = false
    })

    state.items = usersArr
  },
  setId(state, item) {
    state.id = item
  },
  setFirstName(state, item) {
    state.firstName = item
  },
  setLastName(state, item) {
    state.lastName = item
  },
  setEmail(state, item) {
    state.email = item
  },
  setPhone(state, item) {
    state.phone = item
  },
  setCompany(state, item) {
    state.company = item
  }
}

export const getters = {
  getSelected: state => state.items.find(item => item.isSelected === true)
}

export const actions = {
  setItems({ commit }, payload) {
    commit('setItems', payload)
  },
  setSelected({ commit }, payload) {
    commit('setSelected', payload)
  },
  setUser({ commit }, payload) {
    commit('setId', payload.id)
    commit('setFirstName', payload.firstName)
    commit('setLastName', payload.lastName)
    commit('setEmail', payload.email)
    commit('setPhone', payload.phone)
    commit('setCompany', payload.company)
  },
  setId({ commit }, payload) {
    commit('setId', payload)
  },
  setFirstname({ commit }, payload) {
    commit('setFirstName', payload)
  },
  setLastname({ commit }, payload) {
    commit('setLastName', payload)
  },
  setEmail({ commit }, payload) {
    commit('setEmail', payload)
  },
  setPhone({ commit }, payload) {
    commit('setPhone', payload)
  },
  setCompany({ commit }, payload) {
    commit('setCompany', payload)
  }
}
