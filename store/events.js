export const state = () => ({
  items: []
})

export const mutations = {
  setItems(state, item) {
    state.items = item
  },
  setItem(state, item) {
    const index = state.items.map(event => event.id).indexOf(item.id)

    state.items[index] = item
    state.items = [...state.items]
  },
  addItem(state, item) {
    state.items.push(item)
  },
  removeItem(state, item) {
    const event = state.items.find(x => x.id === item)
    const index = state.items.indexOf(event)

    if (index >= 0) state.items.splice(index, 1)
  }
}

export const getters = {
  getItemById: state => id => state.items.find(item => item.id === id)
}

export const actions = {
  setItems({ commit }, payload) {
    commit('setItems', payload)
  },
  setItem({ commit }, payload) {
    commit('setItem', payload)
  },
  addItem({ commit }, payload) {
    commit('addItem', payload)
  },
  removeItem({ commit }, payload) {
    commit('removeItem', payload)
  }
}
